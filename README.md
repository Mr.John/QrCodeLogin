## 扫码登录

目前手机扫描二维码登录已成为一种主流的登录方式，尤其是在 PC 网页端。最近学习了一下扫码登录的原理，感觉蛮有趣的，所以借鉴了网上的一些示例，实现了一个简单的扫码登录的 demo，以此记录一下学习过程。

#### 原理解析

##### 流程简述

1. PC 端打开二维码登录页面 login.html;
2. login.html 调用后端接口 createQrCodeImg，该接口生成一个随机的 uuid，uuid 可看做是本页面的唯一标识，同时该接口还会创建一个 LoginTicket 对象，该对象中封装了如下信息：
 
   - uuid：页面的唯一标识；
   - userId：用户 id；
   - status：扫码状态，0 表示等待扫码，1 表示等待确认，2 表示已确认。 

3. 将上述 uuid 作为 key、LoginTicket 对象作为 value 存储在 Redis 服务器中（或其他数据库），设置其过期时间为 5 分钟，表示 5 分钟后二维码失效。
4. 生成二维码图片，二维码中封装的信息为一个 URL，类似于 http://localhost:8080/login/scan/uuid=1be1cf4a5ceb4d73a8f2104ffe5fba0c 。
5. PC 端显示二维码； 
6. PC 端页面不断轮询（多久轮询一次自行设置）检查扫码的进度，即 LoginTicket 对象的状态。如果为 0 或为 1，继续轮询；如果为 2，停止轮询（已确认登录）；
7. 手机端扫描二维码；
8. 手机端（携带用户的 token，该 token 为手机端 token）访问二维码中的目标网址，手机端服务器首先验证 token 是否有效，如果有效则将 LoginTicket 对象的 status 更新为 1；
9. 手机端服务器询问用户是否确认登录；
10. 用户选择确认登录，手机端服务器将 LoginTicket 对象的 status 更新为 2，并将 userId 设置为当前用户的 id；
11. PC 端检测到用户确认登录后，为用户生成 token（此 token 为 PC 端的 token），并将 token 返回给前端；
12. 前端获取到 token 后就可以执行其他操作。

##### 流程图
总体流程如下：
![扫码登录流程图](src/main/resources/picture/QrCodeLogin.jpg)
##### 代码讲解参见博客：[Join同学来学习](https://segmentfault.com/a/1190000040787531)


